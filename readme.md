# Prepare Homestead : Laradock
```
cd laradock_practice_laravel
docker-compose up -d
```

# Install Laravel package
```sh
cd laravel_demo
cp .env.example .env
# Install php dependency packages
composer install
# Laravel APP Key
php artisan key:generate
# JWT secret key
php artisan jwt:secret
# Create DB migration and fake data
php artisan migrate:refresh --seed
```

# Unit Test

```sh
docker-compose exec workspace bash

cd laravel_demo

# Generate code coverage report in text format
./vendor/bin/phpunit --coverage-text

# Test Feture folder
./vendor/bin/phpunit --testsuite Feture --coverage-text

# Test Unit folder
./vendor/bin/phpunit --testsuite Unit --coverage-text --coverage-html=coverage
```


# 常用指令
```
php artisan list

```


# Open web

確認 /etc/hosts 有加入
``` 
127.0.0.1 laravel_demo.test
```

http://laravel_demo.test