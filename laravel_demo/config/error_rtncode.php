<?php

return [
    // 1xx : User
    101 => 'Email Activation Error.',
    102 => 'Account is still not activated',
    103 => 'Login error, email or password failed!',
];
