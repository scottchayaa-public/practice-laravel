<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DemoSeeder::class);
    }
}

class DemoSeeder extends Seeder
{
    public function run()
    {
        $users = factory(App\Models\Users::class, 5)->create();
        App\Models\Users::create(
            [
                'name' => 'Example',
                'email' => 'example@gmail.com',
                'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
                'remember_token' => str_random(10),
            ]
        );

        $articles = factory(App\Models\Articles::class, 20)->create();

        App\Models\Stocks::create(
            [
            "name" => "demo product",
            "number" => 10
            ]
        );
    }
}
