<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException as ParentHttpException;

class HttpException extends ParentHttpException
{
    private $data;

    public function __construct(int $status, int $code = 0, \Exception $previous = null, array $data = [], array $headers = [], string $message = null)
    {
        $this->data = $data;
        $message = $this->getCodeMessage($status, $code, $message);

        parent::__construct($status, $message, $previous, $headers, $code);
    }

    public function getData()
    {
        return $this->data;
    }

    private function getCodeMessage(int $status, int $code, string $message = null)
    {
        if (config('error_rtncode.' . $code)) {
            return config('error_rtncode.' . $code);
        } else {
            return config('httpstatus.' . $status);
        }

        if ($message) {
            return $this->message;
        } else {
            return 'Undefined error code';
        }
    }
}
