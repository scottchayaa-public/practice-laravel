<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libs\Classes\Common;
use App\Libs\Helpers\CommonHelper;

class LibsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'common', function () {
                return new Common(new CommonHelper);
            }
        );
    }
}
