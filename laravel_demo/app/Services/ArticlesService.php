<?php

namespace App\Services;

use App\Services\Service;
use App\Repositories\ArticlesRepository;

class ArticlesService extends Service
{
    public $articles;

    public function __construct(ArticlesRepository $articles)
    {
        $this->articles = $articles;
    }
}
