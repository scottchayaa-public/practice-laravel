<?php

namespace App\Http\Controllers;

use Common;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class LineController extends Controller
{
    public function __construct()
    {
        $this->access_token = env('LINE_LONG_ACCESS_TOKEN');
    }

    public function reply(Request $request)
    {
        \Log::info(json_encode($request->all()));
        return 'line webhook ok';
    }

    public function webhook(Request $request)
    {
        \Log::info(json_encode([$request->all()]));
        $events = $request->events[0];
        \Log::info(json_encode([$events]));
        $replyToken = $events["replyToken"];

        if ($replyToken == '00000000000000000000000000000000') return 'line webhook ok !';

        $msg = $events["message"]["text"];
        $client = new Client();

        $res = $client->request(
            'POST', 'https://api.line.me/v2/bot/message/reply', [
                'headers' => [
                    'Authorization' => "Bearer $this->access_token"
                ],
                'json' => [
                    'replyToken' => $replyToken,
                    'messages' => [
                        ['type' => "text", 'text' => $msg],
                    ]
                ]
            ]
        );

    }

    public function push(Request $request)
    {
        $to = $request->to;
        $messages = $request->messages;

        $client = new Client();
        $res = $client->request(
            'POST', 'https://api.line.me/v2/bot/message/push', [
                'headers' => [
                    'Authorization' => "Bearer $this->access_token"
                ],
                'json' => [
                    'to' => $to,
                    'messages' => $messages
                ]
            ]
        );

        \Log::info(json_encode($res->getBody()));
    }
}
