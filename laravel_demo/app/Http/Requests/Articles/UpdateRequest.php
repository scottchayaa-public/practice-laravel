<?php

namespace App\Http\Requests\Articles;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'content' => 'string'
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'title',
            'content' => 'content'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute 不得為空',
            'string' => ':attribute 必須為字串',
            'max' => ':attribute 最多為255字元',
        ];
    }
}
