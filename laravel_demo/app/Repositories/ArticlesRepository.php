<?php

namespace App\Repositories;

use App\Models\Articles;
use App\Repositories\Repository;
use App\Exceptions\HttpException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;

class ArticlesRepository extends Repository
{
    protected $articles;

    public function __construct(Articles $articles)
    {
        $this->articles = $articles;
        parent::__construct();
    }

    public function getPaginate(array $inputs, string $sort = 'created_at')
    {
        $result = $this->articles->orderBy($sort, 'desc');

        if (isset($inputs['q'])) {
            $result = $result->where('title', 'like', "%".$inputs['q']."%");
        }

        $size = isset($inputs['size'])?$inputs['size']:config('common.pagesize');
        $page = isset($inputs['page'])?$inputs['page']:0;
        $result = $result->paginate($size, ['*'], 'page', $page);

        return $result;
    }

    public function create(array $inputs)
    {
        return $this->articles->create(['users_id' => Auth::user()->getId()] + $inputs);
    }

    public function update(int $id, array $inputs)
    {
        $article = $this->articles->findOrFail($id);

        if (Auth::user()->cannot('update', $article)) {
            throw new HttpException(403, 0);
        }

        return $article->update($inputs);
    }

    public function delete(int $id)
    {
        $article = $this->articles->findOrFail($id);

        if (Auth::user()->cannot('delete', $article)) {
            throw new HttpException(403, 0);
        }

        return $article->delete();
    }
}
