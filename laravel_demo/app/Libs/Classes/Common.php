<?php

namespace App\Libs\Classes;

use Exception;
use Validator;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Libs\Helpers\CommonHelper;
use Illuminate\Support\Facades\Redis;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Common
{
    /**
     * CommonHelper
     */
    protected $commonHelper;

    /**
     * 初始化
     *
     * @param CommonHelper $commonHelper 依賴注入
     */
    public function __construct(CommonHelper $commonHelper)
    {
        $this->commonHelper = $commonHelper;
    }

    /**
     * 回傳Client IP
     *
     * @return void
     */
    public function getClientIP()
    {
        //如果有ELB則取HTTP_X_FORWARDED_FOR, 如無則取REMOTE_ADDR
        return empty($this->commonHelper->SERVER('HTTP_X_FORWARDED_FOR'))?$this->commonHelper->SERVER('REMOTE_ADDR'):$this->commonHelper->SERVER('HTTP_X_FORWARDED_FOR');
    }

    /**
     * 將一個字符串部分字符用$re替代隱藏
     *
     * @param string $string 待處理的字符串
     * @param int    $start  規定在字符串的何處開始，
     *                       正數 -
     *                       在字符串的指定位置開始
     *                       負數 -
     *                       在從字符串結尾的指定位置開始
     *                       0 -
     *                       在字符串中的第一個字符處開始在字符串中的第一個字符處開始
     * @param int    $length 可選。規定要隱藏的字符串長度。默認是直到字符串的結尾。
     *                       正數 - 從 start
     *                       參數所在的位置隱藏
     *                       負數 -
     *                       從字符串末端隱藏
     * @param string $re     替代符
     *
     * @return string 處理後的字符串
     */
    public static function hidestr($string, $start = 0, $length = 0, $re = '*')
    {
        if (empty($string)) {
            return false;
        }
        $strarr = array();
        $mb_strlen = mb_strlen($string);

        //循環把字符串變為數組
        while ($mb_strlen) {
            $strarr[] = mb_substr($string, 0, 1, 'utf8');
            $string = mb_substr($string, 1, $mb_strlen, 'utf8');
            $mb_strlen = mb_strlen($string);
        }

        $strlen = count($strarr);
        $begin = $start >= 0 ? $start : ($strlen - abs($start));
        $end = $last = $strlen - 1;

        if ($length > 0) {
            $end = $begin + $length - 1;
        }
        elseif ($length < 0) {
            $end -= abs($length);
        }

        for ($i=$begin; $i<=$end; $i++) {
            $strarr[$i] = $re;
        }

        if ($begin >= $end || $begin >= $last || $end > $last) {
            return false;
        }

        return implode('', $strarr);
    }

    /**
     * 判斷資料庫是否連線
     *
     * @return boolean
     */
    public function isDBconnect()
    {
        try {
            $conn = mysqli_connect(
                $this->commonHelper->config('database.connections.mysql.host'),
                $this->commonHelper->config('database.connections.mysql.username'),
                $this->commonHelper->config('database.connections.mysql.password')
            );
            mysqli_close($conn);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Returns json response.
     *
     * @param array|null $payload
     * @param int        $statusCode
     * @param array      $headers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonResponse(array $payload = null, $statusCode = 200, $headers = [])
    {
        $payload = $payload ?: [];
        return response()->json($payload, $statusCode, $headers, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Returns error json response.
     *
     * @param HttpException $e
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonErrorResponse($e)
    {
        $statusCode = $e->getStatusCode();
        $headers = $e->getHeaders();
        $payload = [
            "code" => $e->getCode(),
            "msg"  => $e->getMessage(),
            "data" => method_exists($e, 'getData')?$e->getData():[],
        ];

        if (config("app.debug")) {
            $payload["debug"] = [];
            do {
                array_push(
                    $payload["debug"],
                    [
                    "class" => get_class($e),
                    "file" => $e->getFile(),
                    "line" => $e->getLine()
                    ]
                );
            } while ($e = $e->getPrevious());
        }

        return response()->json($payload, $statusCode, $headers, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Validate request rules
     *
     * @param Request $request
     * @param array   $rules
     * @param array   $messages
     *
     * @return void
     */
    public function validateRequest(Request $request, array $rules, array $messages = [])
    {
        $credentials = $request->only(array_keys($rules));
        $validator = Validator::make($credentials, $rules, $messages);
        if ($validator->fails()) {
            throw new BadRequestHttpException($validator->messages(), null, 101, []);
        }
    }

    /**
     * Redis retry token機制 : 新增/更新 token, 並設定 token ttl 和 token_retry ttl
     *
     * @param string $key
     * @param string $value
     * @param int    $key_ttl
     * @param int    $retry_ttl
     *
     * @return void
     */
    public function setRedisTokenRetry(string $key, string $value, int $key_ttl, int $retry_ttl)
    {
        $key_retry = $key . ':RETRY';
        $ttl_retry = Redis::ttl($key_retry);
        if ($ttl_retry > 0) {
            throw new BadRequestHttpException(
                "Please wait $ttl_retry sec and retry",
                null,
                102,
                ['Retry-After' => $ttl_retry]
            );
        }
        Redis::setex($key_retry, $retry_ttl, 1);
        Redis::setex($key, $key_ttl, $value);
    }

    /**
     * Redis retry token機制 : 刪除 token
     *
     * @param string $key
     *
     * @return void
     */
    public function delRedisTokenRetry(string $key)
    {
        Redis::del($key);
        Redis::del($key . ':RETRY');
    }
}
