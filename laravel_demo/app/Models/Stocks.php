<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Stocks extends Model
{
    protected $table = "stocks";
    protected $primaryKey = 'id';

    protected $fillable = ['name', 'number'];
}
