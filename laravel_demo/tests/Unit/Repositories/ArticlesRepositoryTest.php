<?php

namespace Tests\Unit\Controllers;

use Tests\TestCase;
use App\Models\Users;
use App\Models\Articles;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\ArticlesRepository;

class ArticleRepositoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        // Create auth user
        $user = new Users([
            'id' => 1,
            'name' => 'scott'
        ]);
        $this->be($user);
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /** @test */
    public function getList()
    {
        // arrange
        $input1 = ['q' => 'example'];
        $expected1 = 'Illuminate\Pagination\LengthAwarePaginator';
        $expected2 = 0;

        $target = new ArticlesRepository(new Articles);

        // act
        $actual = $target->getList($input1);

        // assert
        $this->assertInstanceOf($expected1, $actual);
        $this->assertEquals($expected2, $actual->total());
    }

    /** @test */
    public function create()
    {
        // arrange
        $input1 = [];
        $expected = [];

        $mockArticles = $this->initMock(Articles::class);
        $mockArticles->shouldReceive('create')->once()->andReturn($expected);

        $target = app(ArticlesRepository::class);

        // act
        $actual = $target->create($input1);

        // assert
        $this->assertEquals($expected, $actual);
    }
}
