<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticlesTest extends TestCase
{
    /** @test */
    public function ArticlesList()
    {
        /** arrange */
        $url = '/api/articles';

        /** act */
        $response = $this->get($url);

        /** assert */
        $response->assertStatus(200);
    }
}
