<?php

Route::get('/', function () {
    return redirect()->route('dashboard.index');
})->name('index');

Route::group(['namespace' => 'Auth'], function () {
    // Authentication Routes...
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('auth.login');
    Route::post('/logout', 'LoginController@logout')->name('auth.logout');

    // Change Password Routes...
    Route::get('change_password', 'ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
    Route::patch('change_password', 'ChangePasswordController@changePassword')->name('auth.change_password');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('auth.password.reset');
});

// Admin (Backend)
Route::group(['middleware' => ['auth']], function () {
    /**
     * Dashboard
     */
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

    /**
     * Members
     */
    Route::get('/members', 'MembersController@index');


    /**
     * Users
     */
    Route::get('/users', 'UsersController@index');
    Route::get('/users/create', 'UsersController@create');
    Route::post('/users/create', 'UsersController@store');
});
