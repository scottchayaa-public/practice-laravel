<?php

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/line/webhook', 'Line\LineController@webhook');
Route::post('/line/push', 'Line\LineController@push');


Route::get('/line/richmenu', 'Line\LineController@richmenu');
