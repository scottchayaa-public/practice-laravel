<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActionsTable extends Migration
{
    public $tableName = 'user_actions';

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('action', 191);
            $table->string('action_model', 191)->nullable();
            $table->integer('action_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->nullableTimestamps();

            $table->index(["user_id"]);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
