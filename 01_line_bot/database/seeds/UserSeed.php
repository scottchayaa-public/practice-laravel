<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Admin', 'email'  => 'admin@example.com', 'password' => '$2y$10$BWjDEV36iigp5o5IJTTmlOmlllKIeP5evUcdqDoq/ODdaUk9.K8li', 'role_id' => 1, 'remember_token' => null,],
            ['id' => 2, 'name' => 'Scott', 'email'  => 'scott@example.com', 'password' => '$2y$10$BWjDEV36iigp5o5IJTTmlOmlllKIeP5evUcdqDoq/ODdaUk9.K8li', 'role_id' => 2, 'remember_token' => null,],
            ['id' => 3, 'name' => 'User01', 'email' => 'user01@example.com', 'password' => '$2y$10$BWjDEV36iigp5o5IJTTmlOmlllKIeP5evUcdqDoq/ODdaUk9.K8li', 'role_id' => 2, 'remember_token' => null,],
            ['id' => 4, 'name' => 'User02', 'email' => 'user02@example.com', 'password' => '$2y$10$BWjDEV36iigp5o5IJTTmlOmlllKIeP5evUcdqDoq/ODdaUk9.K8li', 'role_id' => 2, 'remember_token' => null,],
            ['id' => 5, 'name' => 'User03', 'email' => 'user03@example.com', 'password' => '$2y$10$BWjDEV36iigp5o5IJTTmlOmlllKIeP5evUcdqDoq/ODdaUk9.K8li', 'role_id' => 2, 'remember_token' => null,],
        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
