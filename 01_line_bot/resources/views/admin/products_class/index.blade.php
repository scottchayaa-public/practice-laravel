@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">庫存類別</li>
        </ol>
    </nav>

    @can('product_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('products_class.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: bold' }}">All</a></li> |
            <li><a href="{{ route('products_class.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: bold' : '' }}">Trash</a></li>
        </ul>
    </p>
    @endcan

    <div class="box">
        <div class="box-header">
            @can('product_create')
            <a href="{{ route('products_class.create') }}" class="btn btn-default">
                <i class="fa fa-plus"></i> 新增
            </a>
            @endcan
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover" >
                <thead>
                    <tr>
                        <th class="col-md-4">類別名稱</th>
                        <th class="col-md-3">備註</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products_class as $row)
                    <tr>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->memo }}</td>
                        <td>
                            {!! view($template, compact('row', 'gateKey', 'routeKey')) !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
    </script>
@endsection
