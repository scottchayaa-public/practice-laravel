@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('products_class.index') }}">庫存類別</a></li>
        <li class="breadcrumb-item active">新增</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('products_class.store') }}">
    {{ csrf_field() }}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">新增庫存類別</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">類別名稱 <span style="color:red">*<span></th>
                    <td>
                        <input type="text" class="form-control" name="name">
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>
                        <input type="text" class="form-control" name="memo">
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button type="submit" class="btn btn-danger">Save</button>
            </p>
        </div>
    </div>
    </form>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


