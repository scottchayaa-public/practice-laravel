@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">庫存查詢</li>
        </ol>
    </nav>

    <div class="box">
        <form id="searchForm" method="GET">
        <div class="box-body">
            <div class="form-group row">
                <label for="type" class="control-label col-md-1">動作：</label>
                <div class="col-md-2">
                    <select name="type" class="form-control" id="type">
                        <option value="">請選擇</option>
                        <option value="1" {{ request('type') == 1 ? 'selected':'' }}>賣出</option>
                        <option value="2" {{ request('type') == 2 ? 'selected':'' }}>進貨</option>
                        <option value="3" {{ request('type') == 3 ? 'selected':'' }}>出貨</option>
                        <option value="4" {{ request('type') == 4 ? 'selected':'' }}>盤點</option>
                    </select>
                </div>
                <label for="type" class="control-label col-md-1">目標對象：</label>
                <div class="col-md-2">
                    <input type="text" name="target_name" class="form-control" value="{{ request('target_name') }}">
                    <a style="font-size:12px;" href="#" data-toggle="modal" data-target="#mVendors">選擇廠商</a>
                </div>
            </div>
            <div class="form-group row">
                <label for="start_created_at" class="control-label col-md-1">時間(起)：</label>
                <div class="col-md-2">
                    <input type="text" name="start_created_at" class="form-control date" value="{{ request('start_created_at') }}">
                </div>
                <label for="start_created_at" class="control-label col-md-1">時間(訖)：</label>
                <div class="col-md-2">
                    <input type="text" name="end_created_at" class="form-control date" value="{{ request('end_created_at') }}">
                </div>
            </div>
        </div>
        </form>
        <div class="box-footer">
            <button id="search" class="btn btn-info"><i class="fa fa-search"></i> 搜尋</button>
            <button id="reset" class="btn btn-warning"><i class="fa fa-eraser"></i> 清除</button>
        </div>
    </div>

    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover" >
                <thead>
                    <tr>
                        <th class="col-md-1">時間</th>
                        <th class="col-md-2">商品名稱</th>
                        <th class="col-md-3">動作 / 對象</th>
                        <th class="col-md-1">進出數量</th>
                        <th class="col-md-1">在庫數量</th>
                        <th class="col-md-1">經手人員</th>
                        <th >備註</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product_logs as $row)
                    <tr>
                        <td>
                            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="{{ $row->created_at }}">
                                {{ $row->created_at->diffForHumans() }}
                            </span>
                        </td>
                        <td>
                            {{ $row->product_name }}<br>
                            <small style="color:#999;">
                            {{ $row->model ?? '' }} / {{ $row->size ?? '' }} / {{ $row->color ?? ''}}
                            </small>
                        </td>
                        <td>{!! $row->type_str !!}{{ empty($row->target_name)?'': " {$row->type_direction} {$row->target_name}"  }}</td>
                        <td>{{ $row->quantity }}</td>
                        <td>{{ $row->stock }}</td>
                        <td>{{ $row->user_name }}</td>
                        <td>{{ $row->memo }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            {!! $product_logs->appends(request()->except('page'))->links() !!}
        </div>
    </div>

<!-- Modal : Vendors -->
<div id="mVendors" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">選擇廠商</h4>
            </div>
            <div class="modal-body">
            @foreach ($vendors as $row)
            <div class="radio">
            <label><input type="radio" name="vendor_name[]" value="{{$row->name}}">{{$row->name}}</label>
            </div>
            @endforeach
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="selectVendor()" data-dismiss="modal">選擇</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
    function selectVendor() {
       $('input[name="target_name"]').val($('input[name="vendor_name[]"]:checked').val());
    }

    function init() {
        const form = $('#searchForm');
        common.initForm(form);

        $('#search').click(function () {
            form.submit();
        });
        $('#reset').click(function () {
            common.resetForm(form);
        });
    }

    init();
</script>
@endsection
