@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/admin/users">帳號管理</a></li>
            <li class="breadcrumb-item active">新增</li>
        </ol>
    </nav>

    <form method="POST" action="/admin/users">
    {{csrf_field()}}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">新增</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th>Name</th>
                    <td>
                        <input type="text" class="form-control" name="name">
                    </td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>
                        <input type="text" class="form-control" name="email">
                    </td>
                </tr>
                <tr>
                    <th>Role</th>
                    <td>
                        <select class="form-control" name="role">
                            @foreach ($roles as $id => $title)
                            <option value="{{ $id }}">{{ $title }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            </table>
        </div>

        <div class="box-footer">
                <input type="submit" class="btn btn-danger" value="Save">
        </div>
    </div>

    </form>

    <br>
    <br>
    <br>
@stop
