@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('vendors.index') }}">廠商管理</a></li>
        <li class="breadcrumb-item active">新增</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('vendors.store') }}">
    {{ csrf_field() }}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">新增廠商資料</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">公司名稱*</th>
                    <td>
                        <input type="text" class="form-control" name="name">
                    </td>
                </tr>
                <tr>
                    <th>統一編號*</th>
                    <td>
                        <input type="text" class="form-control" name="tax_id">
                    </td>
                </tr>
                <tr>
                    <th>公司電話*</th>
                    <td>
                        <input type="text" class="form-control" name="telephone">
                    </td>
                </tr>
                <tr>
                    <th>公司傳真</th>
                    <td>
                        <input type="text" class="form-control" name="fax_number">
                    </td>
                </tr>
                <tr>
                    <th>公司地址</th>
                    <td>
                        <input type="text" class="form-control" name="address">
                    </td>
                </tr>
                <tr>
                    <th>負責人</th>
                    <td>
                        <input type="text" class="form-control" name="contact_person">
                    </td>
                </tr>
                <tr>
                    <th>負責人電話</th>
                    <td>
                        <input type="text" class="form-control" name="contact_telephone">
                    </td>
                </tr>
                <tr>
                    <th>負責人手機</th>
                    <td>
                        <input type="text" class="form-control" name="contact_cellphone">
                    </td>
                </tr>
                <tr>
                    <th>負責人Email</th>
                    <td>
                        <input type="text" class="form-control" name="contact_email">
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>
                        <input type="text" class="form-control" name="memo">
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button type="submit" class="btn btn-danger">Save</button>
            </p>
        </div>
    </div>
    </form>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


