@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">廠商管理</li>
        </ol>
    </nav>

    @can('vendor_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('vendors.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: bold' }}">All</a></li> |
            <li><a href="{{ route('vendors.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: bold' : '' }}">Trash</a></li>
        </ul>
    </p>
    @endcan

    <div class="box">
        <div class="box-header">
            @can('vendor_create')
            <a href="{{ route('vendors.create') }}" class="btn btn-default">
                <i class="fa fa-plus"></i> 新增
            </a>
            @endcan
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-md-2">公司名稱</th>
                        <th class="col-md-1">統一編號</th>
                        <th class="col-md-1">公司電話</th>
                        <th class="col-md-1">公司傳真</th>
                        <th class="col-md-2">公司地址</th>
                        <th class="col-md-2">負責人資訊</th>
                        <th >備註</th>
                        <th class="col-md-1">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vendors as $row)
                    <tr>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->tax_id }}</td>
                        <td>{{ $row->telephone }}</td>
                        <td>{{ $row->fax_number }}</td>
                        <td>{{ $row->address }}</td>
                        <td>
                            {{ $row->contact_person }}<br>
                            {{ $row->contact_telephone }}<br>
                            {{ $row->contact_cellphone }}<br>
                            {{ $row->contact_email }}<br>
                        </td>
                        <td>{{ $row->memo }}</td>
                        <td>{!! view($template, compact('row', 'gateKey', 'routeKey')) !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script>

        // function init() {
        //     const form = $('#searchForm');
        //     common.initForm(form);

        //     $('#search').click(function () {
        //         form.submit();
        //     });
        //     $('#reset').click(function () {
        //         common.resetForm(form);
        //     });
        // }

        // init();
    </script>
@endsection
