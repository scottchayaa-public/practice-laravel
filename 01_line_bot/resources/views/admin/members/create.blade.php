@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('members.index') }}">會員管理</a></li>
            <li class="breadcrumb-item active">新增</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('members.store') }}" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">新增</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">頭像</th>
                    <td></td>
                </tr>
                <tr>
                    <th>非會員編號</th>
                    <td>
                        <input type="text" class="form-control" name="no_card_id" readonly>
                        <button type="button" class="btn btn-xs btn-success">產生編號</button>
                    </td>
                </tr>
                <tr>
                    <th>姓名</th>
                    <td>
                        <input type="text" class="form-control" name="name">
                    </td>
                </tr>
                <tr>
                    <th>性別</th>
                    <td>
                        <input type="radio" name="gender" value="0" checked> 女
                        <input type="radio" name="gender" value="1"> 男
                    </td>
                </tr>
                <tr>
                    <th>出生日期</th>
                    <td>
                        <input type="text" class="form-control date" name="birthday" >
                    </td>
                </tr>
                <tr>
                    <th>試學日期</th>
                    <td>
                        <input type="text" class="form-control date" name="first_try_at" >
                    </td>
                </tr>
                <tr>
                    <th>入校日期</th>
                    <td>
                        <input type="text" class="form-control date" name="in_school_at">
                    </td>
                </tr>
                <tr>
                    <th>電話(H)</th>
                    <td>
                        <input type="text" class="form-control" name="telephone_h" >
                    </td>
                </tr>
                <tr>
                    <th>電話(O)</th>
                    <td>
                        <input type="text" class="form-control" name="telephone_o" >
                    </td>
                </tr>
                <tr>
                    <th>地址</th>
                    <td>
                        <input type="text" class="form-control" name="address" >
                    </td>
                </tr>
                <tr>
                    <th>行動電話(1)</th>
                    <td>
                        <input type="text" class="form-control" name="cellphone_1" >
                    </td>
                </tr>
                <tr>
                    <th>行動電話(2)</th>
                    <td>
                        <input type="text" class="form-control" name="cellphone_2" >
                    </td>
                </tr>
                <tr>
                    <th>緊急聯絡人姓名</th>
                    <td>
                        <input type="text" class="form-control" name="emergency_name" >
                    </td>
                </tr>
                <tr>
                    <th>緊急聯絡人電話</th>
                    <td>
                        <input type="text" class="form-control" name="emergency_phone" >
                    </td>
                </tr>
                <tr>
                    <th>監護人姓名</th>
                    <td>
                        <input type="text" class="form-control" name="guardian_name" >
                    </td>
                </tr>
                <tr>
                    <th>與監護人關係</th>
                    <td>
                        <input type="text" class="form-control" name="guardian_relationship" >
                    </td>
                </tr>
            </table>

            {{-- <div class="row">
                <div class="col-xs-12 form-group">
                    @if ($member->avatar)
                        <a href="{{ asset(env('UPLOAD_PATH').'/'.$member->avatar) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/'.$member->avatar) }}"></a>
                    @endif
                    {!! Form::label('avatar', trans('quickadmin.members.fields.avatar').'', ['class' => 'control-label']) !!}
                    {!! Form::file('avatar', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                    {!! Form::hidden('avatar_max_size', 2) !!}
                    {!! Form::hidden('avatar_max_width', 4096) !!}
                    {!! Form::hidden('avatar_max_height', 4096) !!}
                    <p class="help-block"></p>
                    @if($errors->has('avatar'))
                        <p class="help-block">
                            {{ $errors->first('avatar') }}
                        </p>
                    @endif
                </div>
            </div> --}}

        </div>
    </div>

    <input type="submit" class="btn btn-danger" value="Save">

    </form>

    <br>
    <br>
    <br>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){


        });
    </script>

@stop
