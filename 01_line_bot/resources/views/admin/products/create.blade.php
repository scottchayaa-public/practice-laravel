@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">庫存商品</a></li>
        <li class="breadcrumb-item active">新增</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('products.store') }}">
    {{ csrf_field() }}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">新增庫存商品</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">商品名稱*</th>
                    <td>
                        <input type="text" class="form-control" name="name">
                    </td>
                </tr>
                <tr>
                    <th class="col-md-2">商品類別*</th>
                    <td>
                        <select class="form-control" name="products_class_id">
                            <option value="">請選擇</option>
                            @foreach ($products_class as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-2">進貨廠商*</th>
                    <td>
                        <select class="form-control" name="vendors_id">
                            <option value="">請選擇</option>
                            @foreach ($vendors as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>型號*</th>
                    <td>
                        <input type="text" class="form-control" name="model">
                    </td>
                </tr>
                <tr>
                    <th>尺碼*</th>
                    <td>
                        <input type="text" class="form-control" name="size">
                    </td>
                </tr>
                <tr>
                    <th>顏色*</th>
                    <td>
                        <input type="text" class="form-control" name="color">
                    </td>
                </tr>
                <tr>
                    <th>單價*</th>
                    <td>
                        <input type="number" class="form-control" name="price">
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>
                        <input type="text" class="form-control" name="memo">
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button type="submit" class="btn btn-danger">Save</button>
            </p>
        </div>
    </div>
    </form>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


