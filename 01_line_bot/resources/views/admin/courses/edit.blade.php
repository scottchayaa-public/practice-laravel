@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('courses.index') }}">課程資料</a></li>
        <li class="breadcrumb-item active">編輯</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('courses.update', $course->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">編輯課程資料</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">排序</th>
                    <td>
                        <input type="text" class="form-control" name="sort" value="{{ $course->sort }}">
                    </td>
                </tr>
                <tr>
                    <th class="col-md-2">課程名稱</th>
                    <td>
                        <input type="text" class="form-control" name="name" value="{{ $course->name }}">
                    </td>
                </tr>
                <tr>
                    <th>上課時間(起)</th>
                    <td>
                        <input type="text" class="form-control timepicker" name="start_time_at" value="{{ date('H:i', strtotime($course->start_time_at)) }}">
                    </td>
                </tr>
                <tr>
                    <th>上課時間(迄)</th>
                    <td>
                        <input type="text" class="form-control timepicker" name="end_time_at" value="{{  date('H:i', strtotime($course->end_time_at)) }}">
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>
                        <input type="text" class="form-control" name="memo" value="{{ $course->memo }}">
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button type="submit" class="btn btn-danger">Update</button>
            </p>
        </div>
    </div>
    </form>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection

@section('style')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('javascript')
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
$('.timepicker').timepicker({
    timeFormat: 'HH:mm',
    interval: 30,
    minTime: '09:00',
    maxTime: '22:00',
    dropdown: true,
    scrollbar: true
});
</script>
@endsection
