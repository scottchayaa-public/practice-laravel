@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">課程資料</li>
        </ol>
    </nav>

    @can('course_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('courses.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: bold' }}">All</a></li> |
            <li><a href="{{ route('courses.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: bold' : '' }}">Trash</a></li>
        </ul>
    </p>
    @endcan

    <div class="box">
        <div class="box-header">
            @can('course_create')
            <a href="{{ route('courses.create') }}" class="btn btn-default">
                <i class="fa fa-plus"></i> 新增
            </a>
            @endcan
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-md-1">排序</th>
                        <th class="col-md-2">課程名稱</th>
                        <th class="col-md-2">上課時間</th>
                        <th>備註</th>
                        <th class="col-md-2">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($courses as $row)
                    <tr>
                        <td>{{ $row->sort }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ substr($row->start_time_at, 0, 5) . ' ~ ' . substr($row->end_time_at, 0, 5) }}</td>
                        <td>{{ $row->memo }}</td>
                        <td>{!! view($template, compact('row', 'gateKey', 'routeKey')) !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script>

        // function init() {
        //     const form = $('#searchForm');
        //     common.initForm(form);

        //     $('#search').click(function () {
        //         form.submit();
        //     });
        //     $('#reset').click(function () {
        //         common.resetForm(form);
        //     });
        // }

        // init();
    </script>
@endsection
