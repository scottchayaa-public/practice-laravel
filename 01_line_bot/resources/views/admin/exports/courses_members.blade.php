
<table>
    <thead>
        <tr>
            <th >繳費編號</th>
            <th >班別</th>
            <th >非會員編號</th>
            <th >會員編號</th>
            <th >姓名</th>
            <th >級數</th>
            <th >繳費月份</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($courses_members as $row)
        <tr>
            <td>{{ $row->order_id }}</td>
            <td>{{ $row->course_name }}</td>
            <td>{{ $row->no_card_id }}</td>
            <td>{{ $row->card_id }}</td>
            <td>{{ $row->name }}</td>
            <td>{{ $row->courses_now_level }}</td>
            <td>{{ $row->courses_month_select }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
