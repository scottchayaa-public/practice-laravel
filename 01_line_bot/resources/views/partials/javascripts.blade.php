<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>

<script src="/adminlte/js/bootstrap.min.js"></script>
<script src="/adminlte/js/select2.full.min.js"></script>
<script src="/adminlte/js/main.js"></script>
<script src="/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="/adminlte/plugins/fastclick/fastclick.js"></script>
<script src="/adminlte/js/app.min.js"></script>

{{-- datetimepicker to input:text, class=date --}}
<script src="/adminlte/plugins/datetimepicker/moment-with-locales.min.js"></script>
<script src="/adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>

{{-- Customized js in this project --}}
<script src="/js/common.js"></script>

<script>
    window._token = '{{ csrf_token() }}';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': window._token
        }
    });
</script>

<script>
    $.extend(true, $.fn.dataTable.defaults, {
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/English.json"
        }
    });
</script>

<script>
    $(function(){
        /** add active class and stay opened when selected */
        var url = window.location;

        // for sidebar menu entirely but not cover treeview
        $('ul.sidebar-menu a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');

        $('ul.treeview-menu a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');

        // for treeview
        $('ul.treeview-menu a').filter(function() {
             return this.href == url;
        }).parentsUntil('.sidebar-menu > .treeview-menu').addClass('menu-open').css('display', 'block');

        // Close autocomplete
        $('input:text').attr('autocomplete', 'off');

        // Datetimepicker
        moment.updateLocale('{{ App::getLocale() }}', {
            week: { dow: 1 } // Monday is the first day of the week
        });

        $('input.date').datetimepicker({
            format: "{{ config('app.date_format_moment",
            locale: "zh_tw",
        });

    });
</script>


@yield('javascript')
