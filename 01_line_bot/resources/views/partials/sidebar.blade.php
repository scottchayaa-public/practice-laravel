@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li>
                <a href="/admin">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">資訊看板</span>
                </a>
            </li>

            @can('members_access')
            <li>
                <a href="/admin/members">
                    <i class="fa fa-address-book"></i>
                    <span>會員管理</span>
                </a>
            </li>
            @endcan

            @can('system_management_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>系統管理</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('role_access')
                    <li>
                        <a href="/admin/roles">
                            <i class="fa fa-briefcase"></i>
                            <span>權限管理</span>
                        </a>
                    </li>
                    @endcan

                    @can('user_access')
                    <li>
                        <a href="/admin/users">
                            <i class="fa fa-user"></i>
                            <span>帳號管理</span>
                        </a>
                    </li>
                    @endcan

                    @can('user_action_access')
                    <li>
                        <a href="/admin/user_action">
                            <i class="fa fa-th-list"></i>
                            <span>操作紀錄</span>
                        </a>
                    </li>
                    @endcan

                </ul>
            </li>
            @endcan

            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">密碼變更</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">Logout</span>
                </a>
            </li>
        </ul>
    </section>
</aside>

