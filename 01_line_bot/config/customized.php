<?php

return [
  'pagesize' => 15,

  'line' => [
    'login' => [
        'channel' => env('LINE_LOGIN_CHANNEL', ''),
        'secret' => env('LINE_LOGIN_SECRET', ''),
    ],
    'messaging' => [
        'channel' => env('LINE_MESSAGING_CHANNEL', ''),
        'secret' => env('LINE_MESSAGING_SECRET', ''),
        'long_token' => env('LINE_MESSAGING_LONG_TOKEN', ''),
    ]
  ],
];
