<?php
namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;

    protected $table = 'members';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'email',
        'password'
    ];
    protected $hidden = ['password'];

    public static function boot()
    {
        parent::boot();
    }

    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function line()
    {
        return $this->hasOne('App\LineMember', 'members_id', 'id');
    }
}
