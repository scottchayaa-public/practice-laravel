<?php

namespace App\Http\Controllers\Admin;

use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreMembersRequest;
use App\Http\Requests\Admin\UpdateMembersRequest;

class MembersController extends Controller
{
    protected $gateKey = 'members';

    private function getWhereConditions()
    {
        $conditions = [];
        if (request('email')) {
            $conditions[] = [ 'members.email', 'like', '%'.request('email').'%' ];
        }

        return $conditions;
    }

    public function index(Request $request)
    {
        if (! Gate::allows("{$this->gateKey}_access")) {
            return abort(401);
        }

        $query = Member::query();
        // Show Trash
        if (request('show_deleted') == 1) {
            if (! Gate::allows("{$this->gateKey}_delete")) {
                return abort(401);
            }
            $query->onlyTrashed();
        }

        // Searching
        $members = $query->selectRaw('
            *
        ')
        ->where($this->getWhereConditions())
        ->orderBy('id', 'desc')
        ->paginate(config('customized.pagesize'));

        return view('admin.members.index', [
            'members' => $members,
            'gateKey' => $this->gateKey,
        ]);
    }

    public function create()
    {
        if (! Gate::allows("{$this->gateKey}_create")) {
            return abort(401);
        }
        return view('admin.members.create');
    }

    public function store(StoreMembersRequest $request)
    {
        if (! Gate::allows("{$this->gateKey}_create")) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $member = Member::create($request->all());

        return redirect()->route('members.index');
    }

    public function edit($id)
    {
        if (! Gate::allows("{$this->gateKey}_edit")) {
            return abort(401);
        }
        $member = Member::findOrFail($id);

        return view('admin.members.edit', compact('member'));
    }

    public function update(UpdateMembersRequest $request, $id)
    {
        if (! Gate::allows("{$this->gateKey}_edit")) {
            return abort(401);
        }
        // $request = $this->saveFiles($request);
        $member = Member::findOrFail($id);
        $member->update($request->all());

        return redirect()->route('members.show', $id);
    }

    public function show($id)
    {
        if (! Gate::allows("{$this->gateKey}_view")) {
            return abort(401);
        }

        $member = Member::findOrFail($id);
        $orders = Order::selectRaw('
            orders.id,
            orders.created_at,
            orders.courses_month_select,
            orders.courses_now_level,
            GROUP_CONCAT(courses.name SEPARATOR "<br>") courses_name
        ')
        ->join('order_courses_details', 'order_courses_details.orders_id', '=', 'orders.id')
        ->join('courses', 'courses.id', '=', 'order_courses_details.courses_id')
        ->where('members_id', $id)
        ->orderBy('created_at', 'desc')
        ->groupBy('orders.id')
        ->get();

        return view('admin.members.show', compact('member', 'orders'));
    }

    public function destroy($id)
    {
        if (! Gate::allows("{$this->gateKey}_delete")) {
            return abort(401);
        }
        $member = Member::findOrFail($id);
        $member->delete();

        return redirect()->route('members.index');
    }

    public function massDestroy(Request $request)
    {
        if (! Gate::allows("{$this->gateKey}_delete")) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Member::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public function restore($id)
    {
        if (! Gate::allows("{$this->gateKey}_delete")) {
            return abort(401);
        }
        $member = Member::onlyTrashed()->findOrFail($id);
        $member->restore();

        return redirect()->route('members.index');
    }

    public function perma_del($id)
    {
        if (! Gate::allows("{$this->gateKey}_delete")) {
            return abort(401);
        }
        $member = Member::onlyTrashed()->findOrFail($id);
        $member->forceDelete();

        return redirect()->route('members.index', ['show_deleted' => 1]);
    }
}
