<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Validator;
use App\Course;
use App\Member;
use App\Product;
use App\ProductLog;
use App\MemberPrice;
use App\OrderCoursesDetail;
use App\OrderProductsDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreOrdersRequest;
use App\Http\Requests\Admin\UpdateOrdersRequest;

class OrdersController extends Controller
{
    private function getWhereConditions()
    {
        $conditions = [];
        if (request('card_id')) {
            $conditions[] = [ 'members.card_id', request('card_id') ];
        }
        if (request('no_card_id')) {
            $conditions[] = [ 'members.no_card_id', request('no_card_id') ];
        }
        if (request('member_name')) {
            $conditions[] = [ 'members.name', request('member_name') ];
        }
        if (request('start_created_at')) {
            $conditions[] = [ 'orders.created_at', '>=', request('start_created_at') . ' 00:00:00' ];
        }
        if (request('end_created_at')) {
            $conditions[] = [ 'orders.created_at', '<=', request('end_created_at') . ' 23:59:59' ];
        }

        return $conditions;
    }

    /**
     * 銷售商品 => 扣庫存 => 庫存紀錄
     *
     * @param integer $order_id
     * @param integer $products_id
     * @param integer $quantity
     * @param integer $member_id
     * @return void
     */
    private function sellProducts(int $order_id, int $products_id, int $quantity, int $member_id = null)
    {
        OrderProductsDetail::create([
            'orders_id' => $order_id,
            'products_id' => $products_id,
            'quantity' => $quantity,
        ]);

        ProductLog::create([
            'orders_id' => $order_id,
            'products_id' => $products_id,
            'quantity' => -$quantity,
            'users_id' => Auth::user()->id,
            'type' => 1,
            'stock' => Product::find($products_id)->stock - $quantity,
            'target_name' => empty($member_id) ? null : Member::find($member_id)->name
        ]);

        Product::find($products_id)->decrement('stock', $quantity);
    }

    public function index()
    {
        if (! Gate::allows('order_access')) {
            return abort(401);
        }

        $query = Order::query();
        $template = 'actionsTemplate';
        $gateKey  = 'order_';
        $routeKey = 'orders';

        // Show Trash
        if (request('show_deleted') == 1) {
            if (! Gate::allows('order_delete')) {
                return abort(401);
            }
            $query->onlyTrashed();
            $template = 'restoreTemplate';
        }

        // Searching
        $orders = $query->selectRaw('
            orders.*,
            members.name member_name
        ')
        ->leftJoin('members', 'members.id', '=', 'orders.members_id')
        ->where($this->getWhereConditions())
        ->orderBy('id', 'desc')
        ->paginate(config('common.pagesize'));

        return view('admin.orders.index', compact('orders', 'template', 'gateKey', 'routeKey'));
    }

    public function create()
    {
        if (! Gate::allows('order_create')) {
            return abort(401);
        }

        $member_prices = MemberPrice::all();
        $courses = Course::all();
        $products = Product::orderBy('name')->get();

        return view('admin.orders.create', compact('member_prices', 'courses', 'products'));
    }

    public function store(StoreOrdersRequest $request)
    {
        if (! Gate::allows('order_create')) {
            return abort(401);
        }
        $order = Order::create($request->all());

        return redirect()->route('orders.index');
    }

    public function update(UpdateOrdersRequest $request, $id)
    {
        if (! Gate::allows('order_edit')) {
            return abort(401);
        }
        $order = Order::findOrFail($id);
        $order->update($request->all());

        return redirect()->route('orders.index');
    }

    public function show($id)
    {
        if (! Gate::allows('order_view')) {
            return abort(401);
        }

        $order = Order::selectRaw('
            orders.*,
            members.name member_name
        ')
        ->leftJoin('members', 'members.id', '=', 'orders.members_id')
        ->findOrFail($id);

        $courses = Course::all();
        $products = Product::orderBy('name')->get();
        $order_courses_detail = OrderCoursesDetail::where('orders_id', $id)->get();
        $order_products_detail = OrderProductsDetail::selectRaw('
            *
        ')
        ->join('products', 'products.id', '=', 'order_products_details.products_id')
        ->where('orders_id', $id)->get();

        return view('admin.orders.show', compact('order', 'courses', 'products', 'order_courses_detail', 'order_products_detail'));
    }

    public function destroy($id)
    {
        if (! Gate::allows('order_delete')) {
            return abort(401);
        }
        $order = Order::findOrFail($id);
        $order->delete();

        return redirect()->route('orders.index');
    }

    public function restore($id)
    {
        if (! Gate::allows('order_delete')) {
            return abort(401);
        }
        $order = Order::onlyTrashed()->findOrFail($id);
        $order->restore();

        return redirect()->route('orders.index');
    }

    public function perma_del($id)
    {
        if (! Gate::allows('order_delete')) {
            return abort(401);
        }
        $order = Order::onlyTrashed()->findOrFail($id);
        $order->forceDelete();

        return redirect()->route('orders.index', ['show_deleted' => 1]);
    }

    public function checkoutMember(Request $request)
    {
        \DB::beginTransaction();

        try {
            $member = Member::where('card_id', $request->order["card_id"])->first();

            // Order
            $order = Order::create($request->order + [
                'users_id' => $request->user()->id,
                'members_id' => $member->id
            ]);

            // Order : course details
            foreach ($request->order_courses_details ?? [] as $row) {
                OrderCoursesDetail::create($row + [
                    'orders_id' => $order->id
                ]);
            }

            // Order : product details
            foreach ($request->order_products_details ?? [] as $row) {
                $this->sellProducts($order->id, $row['products_id'], $row['quantity'], $member->id);
            }
        } catch (\Throwable $th) {
            \DB::rollback();
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        \DB::commit();

        return response()->json($order, 200);
    }
    public function checkoutNoneMember(Request $request)
    {
        \DB::beginTransaction();

        try {
            $member = Member::where('no_card_id', $request->order["no_card_id"])->first();

            // Order
            $order = Order::create($request->order + [
                'users_id' => $request->user()->id,
                'members_id' => $member->id
            ]);

            // Order : if member_prices_id has correct value, then create card_id for member
            if (!empty($request->order["member_prices_id"])) {
                if (empty($request->order["assign_card_id"])) {
                    $member->card_id = (Member::max('card_id') ?? 0) + 1;
                } else {
                    $member->card_id = $request->order["assign_card_id"];
                }
                $member->save();
            }

            // Order : course details
            foreach ($request->order_courses_details ?? [] as $row) {
                OrderCoursesDetail::create($row + [
                    'orders_id' => $order->id
                ]);
            }

            // Order : product details
            foreach ($request->order_products_details ?? [] as $row) {
                $this->sellProducts($order->id, $row['products_id'], $row['quantity'], $member->id);
            }
        } catch (\Throwable $th) {
            \DB::rollback();
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        \DB::commit();

        return response()->json($order, 200);
    }

    public function checkoutNewMember(Request $request)
    {
        \DB::beginTransaction();

        try {
            // Member : new
            $validator = Validator::make($request->member, [
                'name' => 'required',
                'first_try_at' => 'required',
                'in_school_at' => 'required',
                'level' => 'required',
            ]);
            $validator->setAttributeNames([
                'name' => '姓名',
                'first_try_at' => '試學日期',
                'in_school_at' => '入校日期',
                'level' => '最後級數',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors()
                ], 422);
            }

            $member = Member::create($request->member);

            // Member : if member_prices_id has correct value, then create card_id for member. Otherwise then create no_crar_id.
            if (!empty($request->order["member_prices_id"])) {
                if (empty($request->order["assign_card_id"])) {
                    $member->card_id = (Member::max('card_id') ?? 0) + 1;
                } else {
                    $member->card_id = $request->order["assign_card_id"];
                }
            } else {
                if (empty($request->order["assign_card_id"])) {
                    $member->no_card_id = (Member::max('no_card_id') ?? 0) + 1;
                } else {
                    $member->no_card_id = $request->order["assign_card_id"];
                }
            }
            $member->save();

            // Order
            $order = Order::create($request->order + [
                'users_id' => $request->user()->id,
                'members_id' => $member->id
            ]);

            // Order : course details
            foreach ($request->order_courses_details ?? [] as $row) {
                OrderCoursesDetail::create($row + [
                    'orders_id' => $order->id
                ]);
            }

            // Order : product details
            foreach ($request->order_products_details ?? [] as $row) {
                $this->sellProducts($order->id, $row['products_id'], $row['quantity'], $member->id);
            }
        } catch (\Throwable $th) {
            \DB::rollback();
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        \DB::commit();

        return response()->json($order, 200);
    }

    public function checkoutGuest(Request $request)
    {
        \DB::beginTransaction();

        try {
            // Order
            $order = Order::create($request->order + [
                'users_id' => $request->user()->id
            ]);

            // Order : product details
            foreach ($request->order_products_details ?? [] as $row) {
                $this->sellProducts($order->id, $row['products_id'], $row['quantity']);
            }
        } catch (\Throwable $th) {
            \DB::rollback();
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        \DB::commit();

        return response()->json($order, 200);
    }

    public function updateOrder(Request $request, $id)
    {
        try {
            // Order
            $order = Order::find($id)->update($request->order);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        return response()->json($order, 200);
    }

    public function updateCoursesDetail(Request $request, $id)
    {
        try {
            // Order
            $order = Order::find($id);
            $payable_price = $order->payable_price
            - $order->courses_other_price
            - $order->courses_rebate_price
            - $order->courses_total_price
            + $request->order["courses_other_price"]
            + $request->order["courses_rebate_price"]
            + $request->order["courses_total_price"];
            $order->update($request->order + ['payable_price' => $payable_price]);

            // Order : course details
            OrderCoursesDetail::where('orders_id', $id)->delete();
            foreach ($request->order_courses_details ?? [] as $row) {
                OrderCoursesDetail::create($row + [
                    'orders_id' => $id
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        return response()->json($order, 200);
    }

    public function updateProductsDetail(Request $request, $id)
    {
        try {
            // Order
            $order = Order::find($id);
            $payable_price = $order->payable_price
            - $order->products_rebate_price
            + $request->order["products_rebate_price"];

            $oldProducts = OrderProductsDetail::selectRaw('
                CAST(products_id as CHAR(20)) products_id,
                CAST(quantity as CHAR(5)) quantity
            ')->where('orders_id', $id)->get();

            $newProducts = $request->order_products_details;

            if (json_encode($oldProducts) != json_encode($newProducts)) {
                $oldProductsPrice = OrderProductsDetail::selectRaw('
                    sum(price * quantity) total
                ')->join('products', 'products.id', '=', 'order_products_details.products_id')
                ->where('orders_id', $id)
                ->first()->total;

                $newProductsPrice = 0;
                foreach ($request->order_products_details as $row) {
                    $newProductsPrice += $row['quantity'] * Product::find($row["products_id"])->price;
                }

                $payable_price = $payable_price - $oldProductsPrice + $newProductsPrice;
            }

            $order->update($request->order + ['payable_price' => $payable_price]);

            // Order : products details
            $order_product_detail = OrderProductsDetail::where('orders_id', $id)->get();
            foreach ($order_product_detail as $row) {
                Product::find($row->products_id)->increment('stock', $row->quantity);
            }
            OrderProductsDetail::where('orders_id', $id)->delete();
            ProductLog::where('orders_id', $id)->delete();
            foreach ($request->order_products_details ?? [] as $row) {
                $this->sellProducts($order->id, $row['products_id'], $row['quantity'], $order->members_id);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        return response()->json($order, 200);
    }
}
