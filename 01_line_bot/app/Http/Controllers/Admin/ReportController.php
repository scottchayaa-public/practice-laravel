<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Course;
use App\Vendor;
use App\ProductLog;
use App\Exports\Export;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class ReportController extends Controller
{

    private function products_getWhereConditions()
    {
        $conditions = [];
        if (request('type')) {
            $conditions[] = [ 'product_logs.type', request('type') ];
        }
        if (request('target_name')) {
            $conditions[] = [ 'product_logs.target_name', 'like', '%' . request('target_name') . '%' ];
        }
        if (request('start_created_at')) {
            $conditions[] = [ 'product_logs.created_at', '>=', request('start_created_at') . ' 00:00:00' ];
        }
        if (request('end_created_at')) {
            $conditions[] = [ 'product_logs.created_at', '<=', request('end_created_at') . ' 23:59:59' ];
        }

        return $conditions;
    }

    private function courses_members_getWhereConditions()
    {
        $conditions = [];
        if (request('courses_id')) {
            $conditions[] = [ 'order_courses_details.courses_id', request('courses_id') ];
        }
        if (request('Ym')) {
            $conditions[] = [ 'orders.courses_month_select', 'like', '%' . request('Ym') . '%' ];
        }

        return $conditions;
    }

    public function products()
    {
        $query = ProductLog::query();

        // Searching
        $product_logs = $query->selectRaw('
            product_logs.*,
            products.name product_name,
            products.model,
            products.size,
            products.color,
            users.name user_name
        ')
        ->where($this->products_getWhereConditions())
        ->join('products', 'products.id', '=', 'product_logs.products_id')
        ->join('users', 'users.id', '=', 'product_logs.users_id')
        ->orderBy('product_logs.id', 'desc')
        ->paginate(config('common.pagesize'));

        $vendors = Vendor::orderBy('name', 'asc')->get();

        return view('admin.report.products', compact('product_logs', 'vendors'));
    }

    public function courses_members()
    {
        $query = Order::query();

        // Searching
        $courses_members = ($this->courses_members_getWhereConditions() == [])
        ? []
        : $query->selectRaw('
            orders.id order_id,
            courses.name course_name,
            members.no_card_id,
            members.card_id,
            members.name name,
            orders.courses_now_level,
            orders.courses_month_select
        ')
        ->join('order_courses_details', 'order_courses_details.orders_id', '=', 'orders.id')
        ->join('courses', 'courses.id', '=', 'order_courses_details.courses_id')
        ->join('members', 'members.id', '=', 'orders.members_id')
        ->where($this->courses_members_getWhereConditions())
        ->orderBy('members.card_id', 'desc')
        ->get();

        $courses = Course::orderBy('name', 'asc')->get();

        return view('admin.report.courses_members', compact('courses_members', 'courses'));
    }

    public function exportCoursesMembers()
    {
        $query = Order::query();

        // Searching
        $courses_members = ($this->courses_members_getWhereConditions() == [])
        ? []
        : $query->selectRaw('
            orders.id order_id,
            courses.name course_name,
            members.no_card_id,
            members.card_id,
            members.name name,
            orders.courses_now_level,
            orders.courses_month_select
        ')
        ->join('order_courses_details', 'order_courses_details.orders_id', '=', 'orders.id')
        ->join('courses', 'courses.id', '=', 'order_courses_details.courses_id')
        ->join('members', 'members.id', '=', 'orders.members_id')
        ->where($this->courses_members_getWhereConditions())
        ->orderBy('members.card_id', 'desc')
        ->get();

        if ($courses_members == []) {
            return '無資料';
        }

        $date = date('Ymd');
        return (new Export(
            [
                '繳費編號', '班別', '非會員編號', '會員編號', '姓名', '級數', '報名月份'
            ],
            $courses_members
        ))->download("班別冊_$date.xlsx");
    }
}
