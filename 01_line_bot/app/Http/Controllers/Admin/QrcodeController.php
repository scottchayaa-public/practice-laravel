<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class QrcodeController extends Controller
{

    public function index()
    {
        if (! Gate::allows('member_access')) {
            return abort(401);
        }

        return view('admin.qrcode.index');
    }

    public function print()
    {
        return view('admin.qrcode.print');
    }
}
