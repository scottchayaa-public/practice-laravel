<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCoursesRequest;
use App\Http\Requests\Admin\UpdateCoursesRequest;
use Yajra\DataTables\DataTables;

class CoursesController extends Controller
{

    public function index()
    {
        if (! Gate::allows('course_access')) {
            return abort(401);
        }

        $query = Course::query();
        $template = 'actionsTemplate';
        $gateKey  = 'course_';
        $routeKey = 'courses';

        // Searching
        if (request('show_deleted') == 1) {
            if (! Gate::allows('course_delete')) {
                return abort(401);
            }
            $query->onlyTrashed();
            $template = 'restoreTemplate';
        }

        $courses = $query->get();

        return view('admin.courses.index', compact('courses', 'template', 'gateKey', 'routeKey'));
    }

    public function create()
    {
        if (! Gate::allows('course_create')) {
            return abort(401);
        }

        $members = \App\Member::get()->pluck('member_id', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        return view('admin.courses.create', compact('members'));
    }

    public function store(StoreCoursesRequest $request)
    {
        if (! Gate::allows('course_create')) {
            return abort(401);
        }
        $course = Course::create($request->all());

        return redirect()->route('courses.index');
    }

    public function edit($id)
    {
        if (! Gate::allows('course_edit')) {
            return abort(401);
        }
        $course = Course::findOrFail($id);

        return view('admin.courses.edit', compact('course'));
    }

    public function update(UpdateCoursesRequest $request, $id)
    {
        if (! Gate::allows('course_edit')) {
            return abort(401);
        }
        $course = Course::findOrFail($id);
        $course->update($request->all());

        return redirect()->route('courses.index');
    }

    public function show($id)
    {
        if (! Gate::allows('course_view')) {
            return abort(401);
        }
        $course = Course::findOrFail($id);

        return view('admin.courses.show', compact('course'));
    }

    public function destroy($id)
    {
        if (! Gate::allows('course_delete')) {
            return abort(401);
        }
        $course = Course::findOrFail($id);
        $course->delete();

        return redirect()->route('courses.index');
    }

    public function restore($id)
    {
        if (! Gate::allows('course_delete')) {
            return abort(401);
        }
        $course = Course::onlyTrashed()->findOrFail($id);
        $course->restore();

        return redirect()->route('courses.index');
    }

    public function perma_del($id)
    {
        if (! Gate::allows('course_delete')) {
            return abort(401);
        }
        $course = Course::onlyTrashed()->findOrFail($id);
        $course->forceDelete();

        return redirect()->route('courses.index', ['show_deleted' => 1]);
    }
}
