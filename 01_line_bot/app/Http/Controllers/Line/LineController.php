<?php

namespace App\Http\Controllers\Line;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Services\Line\PushService;
use App\Services\Line\ReplyService;
use App\Http\Controllers\Controller;
use App\Services\Line\RichmenuService;

class LineController extends Controller
{
    public function __construct(
        PushService $pushService,
        ReplyService $replyService,
        RichmenuService $richmenuService
    ) {
        $this->pushService = $pushService;
        $this->replyService = $replyService;
        $this->richmenuService = $richmenuService;
    }

    public function webhook(Request $request)
    {
        \Log::info(json_encode($request->all(), JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));

        $this->replyService
        ->setEvents($request->events)
        ->parserKeywordReply('小狗', 'puppy')
        ->reply();
    }

    public function push(Request $request)
    {
        $this->pushService->push($request->to, $request->messages);
    }

    public function richmenu(Request $request)
    {
        // $res = $this->richmenuService->create();
        // $res = $this->richmenuService->getList();
        // $res = $this->richmenuService->uploadImage('richmenu-32dbac35fa03b1e553556405fdfac6bb', public_path('/uploads/richmenu/example.jpg'));
        // $res = $this->richmenuService->setDefault('richmenu-32dbac35fa03b1e553556405fdfac6bb');
        $res =  $this->richmenuService->getDefault();
        dd($res);
    }
}
