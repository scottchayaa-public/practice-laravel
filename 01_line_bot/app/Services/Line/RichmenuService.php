<?php

namespace App\Services\Line;

use GuzzleHttp\Client;

/**
 * https://developers.line.biz/en/docs/messaging-api/using-rich-menus/
 */
class RichmenuService
{
    protected $apiUrl_create  = 'https://api.line.me/v2/bot/richmenu';
    protected $apiUrl_content = 'https://api.line.me/v2/bot/richmenu/{richMenuId}/content';
    protected $apiUrl_list    = 'https://api.line.me/v2/bot/richmenu/list';
    protected $apiUrl_setDefault = 'https://api.line.me/v2/bot/user/all/richmenu/{richMenuId}';
    protected $apiUrl_getDefault = 'https://api.line.me/v2/bot/user/all/richmenu';

    /**
     * create richmenu
     *
     * @param array $richmenuObj https://developers.line.biz/en/reference/messaging-api/#rich-menu-object
     * @return string $richMenuId
     */
    public function create(array $richmenuObj)
    {
        $client = new Client;

        $response = $client->post($this->apiUrl_create, [
            'headers' => [
                'Authorization' => 'Bearer ' . config('customized.line.messaging.long_token')
            ],
            'json' => $richmenuObj
        ]);

        $results = json_decode($response->getBody()->getContents(), true);
        return $results['richMenuId'];
    }

    public function uploadImage(string $richMenuId, string $imgPath)
    {
        $client = new Client;

        $response = $client->post(str_replace('{richMenuId}', $richMenuId, $this->apiUrl_content), [
            'headers' => [
                'Authorization' => 'Bearer ' . config('customized.line.messaging.long_token'),
                "Content-Type" => "image/jpeg",
            ],
            'body' => fopen($imgPath, 'r')
        ]);

        $results = json_decode($response->getBody()->getContents(), true);
        return $results;
    }

    public function getList()
    {
        $client = new Client;

        $response = $client->get($this->apiUrl_list, [
            'headers' => [
                'Authorization' => 'Bearer ' . config('customized.line.messaging.long_token')
            ]
        ]);

        $results = json_decode($response->getBody()->getContents(), true);
        return $results;
    }

    public function setDefault(string $richMenuId)
    {
        $client = new Client;

        $response = $client->post(str_replace('{richMenuId}', $richMenuId, $this->apiUrl_setDefault), [
            'headers' => [
                'Authorization' => 'Bearer ' . config('customized.line.messaging.long_token')
            ]
        ]);

        $results = json_decode($response->getBody()->getContents(), true);
        return $results;
    }

    public function getDefault()
    {
        $client = new Client;

        $response = $client->get($this->apiUrl_getDefault, [
            'headers' => [
                'Authorization' => 'Bearer ' . config('customized.line.messaging.long_token')
            ]
        ]);

        $results = json_decode($response->getBody()->getContents(), true);
        return $results;
    }
}
