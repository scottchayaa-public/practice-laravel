<?php

namespace App\Services\Line;

use GuzzleHttp\Client;

class PushService
{
    protected $apiUrl_push = 'https://api.line.me/v2/bot/message/push';

    public function push(string $to, array $messages)
    {
        $client = new Client;

        $response = $client->post($this->apiUrl_push, [
            'headers' => [
                'Authorization' => 'Bearer ' . config('customized.line.messaging.long_token')
            ],
            'json' => [
                'to' => $to,
                'messages' => $messages
            ]
        ]);

        $results = json_decode($response->getBody()->getContents(), true);
        // dd($body);
        return $results;
    }
}
