<?php

namespace App\Services\Line\Login\Callbacks;

use App\Member;
use App\LineMember;
use App\Services\Line\Login\Callbacks\LineLoginCallback;

class CreateMemberCallback extends LineLoginCallback
{
    public function handle(array $profile, array $params)
    {
        if (LineMember::where('uid', $profile['userId'])->count() != 0) {
            return;
        }

        $member = Member::create([]);
        $lineMember = LineMember::create([
            'members_id' => $member->id,
            'uid' => $profile['userId'],
            'displayName' => $profile['displayName'],
            'pictureUrl' => $profile['pictureUrl'],
        ]);
    }
}
