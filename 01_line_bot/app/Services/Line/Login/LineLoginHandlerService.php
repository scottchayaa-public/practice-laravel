<?php

namespace App\Services\Line\Login;

use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;

class LineLoginHandlerService
{
    const LINE_LOGIN_EXPIRED_TIME = 600;
    const LINE_PROFILE_SESSION_KEY = 'lineProfile';

    protected $params;
    protected $profile;

    public function __construct()
    {
    }

    /**
     * 處理Line Login
     *
     * @param string $code
     * @param string $state 此處state為我們加密後的字串，解開可以得到額外傳遞json字串
     * @return void
     */
    public function login(string $code, string $state): void
    {
        $this->decryptParamsFromState($state);
        $this->getProfile($code);
        $this->setProfileToSession();

        if (!empty($this->params['callback'])) {
            $callback = new $this->params['callback'];
            $callback->handle($this->profile, $this->params['data']);
        }

        if (!empty($this->params['redirectPageUrl'])) {
            header('Location: ' . $this->params['redirectPageUrl']);
        }
    }

    /**
     * state 解密
     *
     * @param string $state
     * @return void
     */
    protected function decryptParamsFromState(string $state): void
    {
        $json = decrypt($state);
        $this->params = json_decode($json, true);
        if (empty($this->params)) {
            throw new \Exception('Parameters not found.');
        }
    }

    protected function jwtParse($id_token)
    {
        JWT::$leeway = 60;
        return JWT::decode($id_token, config('customized.line.login.secret'), ['HS256']);
    }

    /**
     * code 打GW取 profile
     *
     * @param string $code
     * @return void
     */
    protected function getProfile(string $code): void
    {
        $redirectUrl = $this->params['redirectUrl'];

        $client = new Client;
        $response = $client->post('https://api.line.me/oauth2/v2.1/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => $redirectUrl,
                'client_id' => config('customized.line.login.channel'),
                'client_secret' => config('customized.line.login.secret')
            ]
        ]);

        $results = json_decode($response->getBody()->getContents());
        $payload = $this->jwtParse($results->id_token);

        $this->profile = [
            'displayName' => $payload->name,
            'userId' => $payload->sub,
            'pictureUrl' => $payload->picture,
            'statusMessage' => '',
            'email' => $payload->email ?? '',
            'phone' => $payload->phone ?? '',
        ];
    }

    /**
     * 登入完後，將profile寫入SESSION
     *
     * @return void
     */
    protected function setProfileToSession(): void
    {
        Session::put(static::LINE_PROFILE_SESSION_KEY, $this->profile, static::LINE_LOGIN_EXPIRED_TIME);
    }
}
