<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class LineMember extends Model
{
    use Notifiable;

    protected $table = 'line_members';
    protected $primaryKey = 'members_id';

    protected $fillable = [
        'members_id',
        'displayName',
        'uid',
        'pictureUrl',
        'friendFlag',
        'email',
        'phone',
    ];
    protected $hidden = [];

    public static function boot()
    {
        parent::boot();
    }
}
